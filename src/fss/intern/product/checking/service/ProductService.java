package fss.intern.product.checking.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import fss.intern.product.checking.dao.ProductDAO;
import fss.intern.product.checking.dao.ProductDAOImpl;
import fss.intern.product.checking.entities.Product;

/**
 * @author Huong
 *
 */
public class ProductService {

	List<Product> errPUrl_DB = new ArrayList<Product>();
	List<Product> errPUrl_CSV = new ArrayList<Product>();
	List<Product> errPCode_DB = new ArrayList<Product>();
	List<Product> errPCode_CSV = new ArrayList<Product>();

	private ProductDAO pd;

	public ProductService(ProductDAOImpl pd) {
		super();
		this.pd = pd;
	}

	/*
	 * Function insert product_url and product_code from csv to db
	 */
	public boolean insertCSVtoDB(String csvFilePath) {
		LinkedList<Product> products = getContentCSV(csvFilePath);
		return pd.insertAll(products);
	}

	/*
	 * get all product from db save into HashMap<product_url, product_code>
	 */
	public List<Product> getAllProductFromDB() {
		return pd.getAll();
	}

	public List<Product> getProductListFromDBByLimit(int from, int to) {
		System.out.println("Start execute sql to get data!");
		return pd.getDataByLimit(from, to);
	}

	public int getDBSize() {
		return pd.getDBSize();
	}

	public HashMap<String, Product> getAllProductUrl(List<Product> pList) {
		HashMap<String, Product> pUrlList = new HashMap<String, Product>();
		for (Product product : pList) {
			pUrlList.put(product.getpUrl(), product);
		}
		return pUrlList;
	}

	public HashMap<String, Product> getAllProductCode(List<Product> pList) {
		HashMap<String, Product> pCodeList = new HashMap<String, Product>();
		for (Product product : pList) {
			pCodeList.put(product.getpCode(), product);
		}
		return pCodeList;
	}

	/*
	 * Function check what's column contain product_url and product_code product
	 * variable: a row data in csv return [index of column contain product_url,
	 * index of column contain product_code]
	 */
	private int[] findColumnIndexInCSVByRow0(String[] columnName) {
		int[] pIndex = new int[3];
		int pUrlIndex = 0;
		int pCodeIndex = 0;
		int pNameIndex = 0;

		boolean pUrlIndexFound = false;
		boolean pCodeIndexFound = false;
		boolean pNameIndexFound = false;
		for (int i = 0; i < columnName.length; i++) {
			if (!pUrlIndexFound && columnName[i].toLowerCase().contains("url")) {
				pUrlIndex = i;
				pUrlIndexFound = true;
			}
			if (!pCodeIndexFound && (columnName[i].toLowerCase().contains("code")
					|| (columnName[i].toLowerCase().contains("id")) && (i != 0))) {
				pCodeIndex = i;
				pCodeIndexFound = true;
			}
			if (!pNameIndexFound && columnName[i].toLowerCase().contains("name")) {
				pNameIndex = i;
				pNameIndexFound = true;
			}
			if (pUrlIndexFound && pCodeIndexFound && pNameIndexFound) {
				pIndex[0] = pUrlIndex;
				pIndex[1] = pCodeIndex;
				pIndex[2] = pNameIndex;
				return pIndex;
			}
		}
		return pIndex;
	}

	/*
	 * Function get content of csv on local
	 */
	public LinkedList<Product> getContentCSV(String csvFilePath) {
		LinkedList<Product> products = new LinkedList<>();

		String line = "";
		String cvsSplitBy = ",";
		BufferedReader buffer = null;
		File file = new File(csvFilePath);

		try {
			buffer = new BufferedReader(new FileReader(file));

			int rowNum = 0;
			int[] index = new int[3];
			System.out.println("Getting content from csv..." + csvFilePath);
			while ((line = buffer.readLine()) != null) {
				// use comma as separator
				String[] product = line.split(cvsSplitBy);
				if (rowNum == 0) {
					index = findColumnIndexInCSVByRow0(product);
					rowNum++;
					continue;
				}

				if (product.length > Common.max(index)) {
					String pUrlPartternStr = "^(http[s]?://?|www)";
					String pCodePartternStr = "^[a-z0-9]+-([a-z0-9]+-)*[a-z0-9]+$";
					Pattern pUrlParttern = Pattern.compile(pUrlPartternStr);
					Pattern pCodeParttern = Pattern.compile(pCodePartternStr);

					String pUrl = product[index[0]].contains("\"") ? product[index[0]].replace("\"", "")
							: product[index[0]];
					String pCode = product[index[1]].contains("\"") ? product[index[1]].replace("\"", "")
							: product[index[1]];
					String pName = product[index[2]].contains("\"") ? product[index[2]].replace("\"", "")
							: product[index[2]];

					if (pUrlParttern.matcher(pUrl).find() && pCodeParttern.matcher(pCode).find()) {
						// add product to link list
						Product p = new Product((rowNum + 1), pUrl, pCode, pName);
						products.add(p);
					}
					rowNum++;
				}
			}
			System.out.println("Get content csv completed --> " + csvFilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} finally {
//			buffer.close();
			// delete file after got content to check product
//			if (file.exists()) {
//				 file.delete();
//				 System.out.println("Deleted file after get content!!!");
//			}
		}
		return products;
	}

	/*
	 * Function to send mail
	 */
	public void sendMail(String from, String to, String pwd, String subject, String message) {
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.starttls.required", "true");
		final String login = from;
		// final String pwd = "HoaHuowngf26";
		Authenticator pa = null;
		if (login != null && pwd != null) {
			props.put("mail.smtp.auth", "true");
			pa = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(login, pwd);
				}
			};
		} // else: no authentication
		Session session = Session.getInstance(props, pa);
		// Create a new message
		Message msg = new MimeMessage(session);
		try {
			// Set the FROM and TO fields
			msg.setFrom(new InternetAddress(from));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
			msg.setContent(message, "text/html; charset=UTF-8");
			msg.setSubject(subject);
			msg.setText(message);
			msg.setHeader("Content-Type", "text/plain; charset=UTF-8");
			msg.setSentDate(new Date());
			msg.saveChanges();
			Transport.send(msg);
			System.out.println("Sent mail successfully!");
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * productList_DB: all product got from DB productList_CSV: all product got
	 * from 1 file CSV return mail content is not empty if product get error
	 * code or url
	 */

	public String checkAllProduct(List<Product> productList_DB, List<Product> productList_CSV) {
		String resultMsg = "";

		Map<String, Product> pUrlList = getAllProductUrl(productList_DB);
		Map<String, Product> pCodeList = getAllProductCode(productList_DB);

		LinkedList<Product> newProducts = new LinkedList<Product>();
		System.out.println("Checking...");
		for (Product product : productList_CSV) {
			// not found product with url and code --> product is new
			if ((pCodeList.get(product.getpCode()) == null) && (pUrlList.get(product.getpUrl()) == null)) {
				// pd.insertOne(product);
				newProducts.add(product);
			}
			// not found product by code
			else if ((pCodeList.get(product.getpCode()) == null) && (pUrlList.get(product.getpUrl()) != null)) {
				errPCode_DB.add(pUrlList.get(product.getpUrl()));
				errPCode_CSV.add(product);
			}
			// not found product by url
			else if ((pCodeList.get(product.getpCode()) != null) && (pUrlList.get(product.getpUrl()) == null)) {
				// if not found product by url and not found product by name
				Product p = pCodeList.get(product.getpCode());
				if (!p.getpName().equals(product.getpName())) {
					errPUrl_DB.add(p);
					errPUrl_CSV.add(product);
				}
			}
		}
		if (!newProducts.isEmpty()) {
			try {
				pd.insertAll(newProducts);
			} catch(Exception e) {
				System.out.println("[WARMING]: " + e.getMessage());
			}
			
		}
		resultMsg = generateContentMail(errPUrl_DB, errPUrl_CSV, errPCode_DB, errPCode_CSV);
		return resultMsg;
	}

	/*
	 * function generate mail content with product info which incorrect product
	 * url or product code
	 */
	public String generateContentMail(List<Product> errPUrlDB, List<Product> errPUrlCSV, List<Product> errPCodeDB,
			List<Product> errPCodeCSV) {
		String msg = "";
		if (errPUrlDB.size() != 0) {
			msg += "█ URL間違いリスト & Prodct name間違い：\n";
			for (int i = 0; i < errPUrlDB.size(); i++) {
				msg += "▼" + (i + 1) + "\n";
				msg += "DB上データ：\n" + errPUrlDB.get(i).getpUrl() + " | " + errPUrlDB.get(i).getpCode() + " | "
						+ errPUrlDB.get(i).getpName() + "\n";
				msg += "CSV上データ：\n" + "（" + errPUrlCSV.get(i).getRowId() + "行目）\n" + errPUrlCSV.get(i).getpUrl() + " | "
						+ errPUrlCSV.get(i).getpCode() + " | " + errPUrlCSV.get(i).getpName() + "\n";
			}
		}
		if (errPCodeDB.size() != 0) {
			msg += "█ ProductID間違いリスト：\n";
			for (int i = 0; i < errPCodeDB.size(); i++) {
				msg += "▼" + (i + 1) + "\n";
				msg += "DB上データ：\n" + errPCodeDB.get(i).getpUrl() + " | " + errPCodeDB.get(i).getpCode() + "\n";
				msg += "CSV上データ：\n" + "（" + errPCodeCSV.get(i).getRowId() + "行目）\n" + errPCodeCSV.get(i).getpUrl()
						+ " | " + errPCodeCSV.get(i).getpCode() + "\n";
			}
		}
		
		this.errPCode_CSV.removeAll(errPCodeCSV);
		this.errPCode_DB.removeAll(errPCodeDB);
		this.errPUrl_CSV.removeAll(errPUrlCSV);
		this.errPUrl_DB.removeAll(errPUrlDB);
		return msg;
	}

	public void downloadCSVAndInsertToDB(String serverPath, String localPath) {
		// MULTI-DOWNFILE
		Crawler cl = new Crawler();
		List<FolderLink> fl = cl.getFolderLinkList(serverPath, localPath);
		List<CsvDownloader> downloaderList = new ArrayList<CsvDownloader>();
		HashMap<FolderLink, List<FileLink>> fileHashMap = cl.getFileLinkList(fl);
		Iterator<?> itf = fileHashMap.entrySet().iterator();
		while (itf.hasNext()) {
			Map.Entry pair = (Map.Entry) itf.next();
			// System.out.println(pair.getValue());
			CsvDownloader cd = new CsvDownloader((List<FileLink>) pair.getValue());
			downloaderList.add(cd);
		}

		// start Thread
		Iterator<CsvDownloader> itDownloader = downloaderList.iterator();
		while (itDownloader.hasNext()) {
			CsvDownloader cd = itDownloader.next();
			cd.start();
		}

		// while to check when Thread is finish
		while (true) {
			Iterator<CsvDownloader> itDownloaderCsv = downloaderList.iterator();
			while (itDownloaderCsv.hasNext()) {
				CsvDownloader cd = itDownloaderCsv.next();
				if (!cd.isAlive()) { // if Thread is finished
					System.out.println("DOWNLOAD FINISHED: " + cd.getFileLinkList());
					// insert csv data to db
					boolean insertCompleted = insertCSVtoDB(cd.getFileLinkList().get(0).getDestination());
					if (insertCompleted) {
						System.out.println("Inserted file " + cd.getFileLinkList().get(0).getName());
						// delete file after inserted
						File f = new File(cd.getFileLinkList().get(0).getDestination());
						f.delete();
					}
					// remove thread from list if it is finished
					itDownloaderCsv.remove();

				}
			}

			// break loop when all thread is remove
			if (downloaderList.size() == 0) {
				break;
			}
		}
	}

	public void downloadAndCheck(String serverPath, String localPath, ProductService ps) {
		// MULTI-DOWNFILE
		Crawler cl = new Crawler();
		List<FolderLink> fl = cl.getFolderLinkList(serverPath, localPath);
		List<CsvDownloader> downloaderList = new ArrayList<CsvDownloader>();
		HashMap<FolderLink, List<FileLink>> fileHashMap = cl.getFileLinkList(fl);
		Iterator<?> itf = fileHashMap.entrySet().iterator();
		while (itf.hasNext()) {
			Map.Entry pair = (Map.Entry) itf.next();
			// System.out.println(pair.getValue());
			CsvDownloader cd = new CsvDownloader((List<FileLink>) pair.getValue());
			downloaderList.add(cd);
		}

		// start Thread
		Iterator<CsvDownloader> itDownloader = downloaderList.iterator();
		while (itDownloader.hasNext()) {
			CsvDownloader cd = itDownloader.next();
			cd.start();
		}

		checkProductAfterDownloadedCSV(downloaderList, ps);
	}

	private void checkProductAfterDownloadedCSV(List<CsvDownloader> downloaderList, ProductService ps) {
		// while to check when Thread is finish
		while (true) {
			Iterator<CsvDownloader> itDownloaderCsv = downloaderList.iterator();
			while (itDownloaderCsv.hasNext()) {
				CsvDownloader cd = itDownloaderCsv.next();
				List<Product> csvData = ps.getContentCSV(cd.getFileLinkList().get(0).getDestination());
				if (!cd.isAlive()) { // if Thread is finished
					System.out.println("DOWNLOAD FINISHED: " + cd.getFileLinkList().get(0).getHref());
					// check csv data with db data after that send mail if
					// product get error
					StringBuilder mailContent = new StringBuilder();
					int size = pd.getDBSize();
					int offset = size/100 + (size%100);
					System.out.println("Start all product checking file: [" + cd.getFileLinkList().get(0).getName() + "] ...!");
					for (int i = 0; i < 100; i++) {
						int from = i * offset + 1;
						String tempMailContent = ps.checkAllProduct(getProductListFromDBByLimit(from, offset), csvData);
						mailContent.append(tempMailContent);
					}
					System.out.println("End product checking file [" + cd.getFileLinkList().get(0).getName() + "] ...with result_length: " + mailContent.length());
					if (!mailContent.toString().isEmpty()) {
						System.out.println(mailContent.toString());
						Common.writeCheckingResultLog(mailContent.toString(), cd.getFileLinkList().get(0).getName().split(".csv")[0]);
						// Mail addr of Taneichi-san:
						// taneichi.takafumi@feelsyncsystem.vn
//						ps.sendMail("gnouh.it@gmail.com", "thducuit@gmail.com", "email_password",
//								"Test send mail: 間違いプロダクト" + " at file name: " + cd.getFileLinkList().get(0).getName(),
//								mailContent.toString());
						mailContent = new StringBuilder();
					}else{
						System.out.println("\nData is not changed\n");
					}
					// delete file after checking
					File f = new File(cd.getFileLinkList().get(0).getDestination());
					if(f.exists()) {
						f.delete();
						System.out.println("Deleted file after checking");
					}
					csvData.clear();
					// remove thread from list if it is finished
					itDownloaderCsv.remove();
				}
			}

			// break loop when all thread is remove
			if (downloaderList.size() == 0) {
				break;
			}
		}
	}
}
