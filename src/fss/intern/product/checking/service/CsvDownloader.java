package fss.intern.product.checking.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class CsvDownloader extends Thread {

	private List<FileLink> fileLinkList;

	public CsvDownloader(List<FileLink> fl) {
		super();
		this.fileLinkList = fl;
	}
	
	public List<FileLink> getFileLinkList() {
		return fileLinkList;
	}

	public void setFileLinkList(List<FileLink> fileLinkList) {
		this.fileLinkList = fileLinkList;
	}

	public void run() {
		Iterator<FileLink> it = this.fileLinkList.iterator();
		while (it.hasNext()) {
			FileLink fl = (FileLink) it.next();

			URL url;
			BufferedInputStream bis = null;
			FileOutputStream fos = null;
			try {
				if(fl != null) {
					url = new URL(fl.getHref());
					bis = new BufferedInputStream(url.openStream());
					File f = new File(fl.getDestination());
					if (!f.exists()) {
						f.createNewFile();
					}
					fos = new FileOutputStream(f);
					byte[] buffer = new byte[1024];
					int count = 0;
					System.out.println("Start download file ..." + fl.getHref());
					while ((count = bis.read(buffer, 0, 1024)) != -1) {
						fos.write(buffer, 0, count);
					}
					fos.flush();
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if(fos != null) {
						fos.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
