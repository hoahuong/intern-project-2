/**
 * 
 */
package fss.intern.product.checking.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * @author Huong
 *
 */
public class Common {

	private static String destinationPath = /*System.getProperty("user.dir") + */"/var/www/html/";

	public static void writeCheckingResultLog(String content, String prefixFileName) {
		BufferedWriter output = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();

		try {
			String filePath = destinationPath + prefixFileName + ".txt";// + "_" + dateFormat.format(date) + ".txt";
			System.out.println(filePath);
			File file = new File(filePath);
			if (!file.exists()) {
				file.createNewFile();
			}
			output = new BufferedWriter(new FileWriter(file));
			output.write(content);
			System.out.println("Created checking_result file: " + filePath);
		} catch (IOException e) {
			System.out.println("[ERROR]: " + e.getMessage());
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static FileLink getTheLastestFileName(ArrayList<FileLink> fileList) {
		FileLink theLastestFileName = null;
		int dateNum = 0;
		for (int i = 0; i < fileList.size(); i++) {
			String fName = fileList.get(i).getName();
			String date = fName.split("_")[1].split(".csv")[0];
			if ((dateNum < Integer.parseInt(date)) && (i < (fileList.size() - 1))) {
				dateNum = Integer.parseInt(date);
			} else {
				theLastestFileName = fileList.get(i);
			}
		}
		return theLastestFileName;
	}

	/*
	 * get key by value from hashmap
	 */
	public static Integer getKey(Map<Integer, String> pUrlList, String value) {
		for (Integer key : pUrlList.keySet()) {
			if (pUrlList.get(key).equals(value)) {
				return key; // return the first found
			}
		}
		return 0;
	}

	/*
	 * Function check file existed or not
	 */
	public static boolean fileIsExisted(String filePath) {
		File file = new File(filePath);
		if (file.exists()) {
			return true;
		}
		return false;
	}

	public static int max(int[] nums) {
		int max = 0;
		for (int i = 0; i < nums.length; i++) {
			if (max < nums[i]) {
				max = nums[i];
			}
		}
		return max;
	}

	private final static Logger logger = Logger.getLogger("Memory Infomation");

	public static String getMemoryInfo() {
		String info = "";

		DecimalFormat format_mem = new DecimalFormat("#,###KB");
		DecimalFormat format_ratio = new DecimalFormat("##.#");
		long free = Runtime.getRuntime().freeMemory() / 1024;
		long total = Runtime.getRuntime().totalMemory() / 1024;
		long max = Runtime.getRuntime().maxMemory() / 1024;
		long used = total - free;
		double ratio = (used * 100 / (double) total);

		info += "Total   = " + format_mem.format(total);
		info += "\n";
		info += "Free    = " + format_mem.format(total);
		info += "\n";
		info += "use     = " + format_mem.format(used) + " (" + format_ratio.format(ratio) + "%)";
		info += "\n";
		info += "can use = " + format_mem.format(max);
		return info;
	}

	public static void viewMemoryInfo() {
		FileHandler fh;
		try {
			fh = new FileHandler("C://logs/MyLogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("---------- MemoryInfo --------");
		logger.info(getMemoryInfo());
		logger.info("------------------------------");
	}
}
