package fss.intern.product.checking.service;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {
	
//	private String destination = "C:\\csv\\";
	
	private List<FolderLink> folderLinkList = new ArrayList<FolderLink>();
	
	private HashMap<FolderLink, List<FileLink>> fileHashMap = new HashMap<FolderLink, List<FileLink>>();
	
	public List<FolderLink> getFolderLinkList(String URI, String localPath) {
		//new File(destination).mkdirs();
		Document doc;
		try {
			doc = Jsoup.connect(URI).userAgent("Mozilla/5.0").get();
			Elements elements = doc.select("pre a");
			
			for (Element element : elements) {
				String linkHref = element.attr("href");
				String linkText = element.text();
				if(this.isFolderLink(linkHref)) {
					String name = linkText.substring(0, linkText.length()-1);
					String href = URI + linkHref;
					String destination = localPath;
					FolderLink fl = new FolderLink(name, href,  destination);
					this.folderLinkList.add(fl);
				}
			}
		} catch(Exception ex) {
			
		}
		return this.folderLinkList;
	}
	
	private boolean isFolderLink(String href) {
		return href.matches("^[a-zA-Z0-9]+/$");
	}
	
	public HashMap<FolderLink, List<FileLink>> getFileLinkList(List<FolderLink> list) {
		Document doc;
		Iterator<FolderLink> it = list.iterator();
		while (it.hasNext()) {
			FolderLink fl = it.next();
			
			ArrayList<FileLink> listFileLink = new ArrayList<FileLink>();
			try {
				doc = Jsoup.connect(fl.getHref()).userAgent("Mozilla/5.0").get();
				Elements elements = doc.select("pre a");
				ArrayList<FileLink> listFileLinkTemp = new ArrayList<FileLink>();
				for (Element element : elements) {
					String linkHref = element.attr("href");
					String linkText = element.text();
					
					if(this.isCsvLink(linkHref)) {
						String name = linkText;
						String href = fl.getHref() + linkHref;
						String destination = fl.getDestination() + name;
						FileLink f = new FileLink(name, href,  destination);
						listFileLinkTemp.add(f);
						System.out.println(f);
					}
				}

				listFileLink.add(Common.getTheLastestFileName(listFileLinkTemp));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			this.fileHashMap.put(fl, listFileLink);
		}
		
		return this.fileHashMap;
	}
	
	private boolean isCsvLink(String href) {
		return href.indexOf(".csv") > 0;
	}
}
