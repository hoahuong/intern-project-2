package fss.intern.product.checking.service;

/**
 * @author Huong
 *
 */
public class FolderLink {
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	private String href;
	private String destination;
	public FolderLink(String name, String href, String destination) {
		super();
		this.name = name;
		this.href = href;
		this.destination = destination;
	}
	@Override
	public String toString() {
		return "FolderLink [name=" + name + ", href=" + href + ", destination=" + destination + "]";
	}
	
	
}
