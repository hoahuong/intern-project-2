package fss.intern.product.checking.dao;

import java.util.LinkedList;
import java.util.List;

import fss.intern.product.checking.entities.Product;

/**
 * @author Huong
 *
 */
public interface ProductDAO {
	
	//insert all products from csv to product table
	public boolean insertAll(LinkedList<Product> products);
	//insert one product from csv to product table
	public void insertOne(Product product);
	//find product by product_code
	public Product findProductByCode(String code);
	//find product by product_url
	public Product findProductByUrl(String url);
	//get all product from db save to map<product_url, product_code>
	public List<Product> getAll();
	public List<Product> getDataByLimit(int from, int to);
	public int getDBSize();
}
