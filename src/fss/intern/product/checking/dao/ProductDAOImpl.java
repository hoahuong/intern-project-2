/**
 * 
 */
package fss.intern.product.checking.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import fss.intern.product.checking.entities.Product;
import fss.intern.product.checking.service.Common;
import fss.intern.product.checking.utils.MySQLConnUtils;

/**
 * @author Huong
 *
 */
public class ProductDAOImpl implements ProductDAO {

	@Override
	public boolean insertAll(LinkedList<Product> products) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			String sql = "insert into product(url, product_code, product_name) values(?,?,?)";
			ps = conn.prepareStatement(sql);

			final int batchSize = 5000;
			int count = 0;

			for (Product product : products) {
				ps.setString(1, product.getpUrl());
				ps.setString(2, product.getpCode());
				ps.setString(3, product.getpName());
				ps.addBatch();

				if ((++count % batchSize == 0) || (count == products.size())) {
					ps.executeBatch();
				}
			}
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	@Override
	public void insertOne(Product product) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			conn.setAutoCommit(false);
			String sql = "insert into product(row_id, url, product_code) values(?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, product.getRowId());
			ps.setString(2, product.getpUrl());
			ps.setString(3, product.getpCode());
			ps.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public Product findProductByCode(String code) {
		Product product = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from product where product_code = " + "\'" + code + "\'");
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					// product = new Product(rs.getInt("row_id"),
					// rs.getString("url"), rs.getString("product_code"));
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return product;
	}

	@Override
	public Product findProductByUrl(String url) {
		Product product = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select * from product where url = " + "\'" + url + "\'");
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					// product = new Product(rs.getInt("row_id"),
					// rs.getString("url"), rs.getString("product_code"));
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return product;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fss.intern.product.checking.dao.ProductDAO#getAll()
	 * HashMap<product_url, product_code>
	 */
	public List<Product> getAll() {
		List<Product> pLists = new LinkedList<Product>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select url, product_code, product_name from product");
			ps.setFetchSize(Integer.MIN_VALUE);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					String pUrl = rs.getString("url");
					String pCode = rs.getString("product_code");
					String pName = rs.getString("product_name");
					pLists.add(new Product(pUrl, pCode, pName));
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		System.out.println("Got all data!");
		return pLists;
	}

	@Override
	public List<Product> getDataByLimit(int from, int limit) {
		// TODO Auto-generated method stub
		List<Product> pLists = new LinkedList<Product>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select url, product_code, product_name from product LIMIT " + from + "," + limit);
			ps.setFetchSize(Integer.MIN_VALUE);
			rs = ps.executeQuery();
			try {
				while (rs.next()) {
					String pUrl = rs.getString("url");
					String pCode = rs.getString("product_code");
					String pName = rs.getString("product_name");
					pLists.add(new Product(pUrl, pCode, pName));
				}
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		System.out.println("Data(DB) FROM: " + from + " limit:" + limit);
		return pLists;
	}

	@Override
	public int getDBSize() {
		int dbSize = 0;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = MySQLConnUtils.getMySQLConnection();
			ps = conn.prepareStatement("Select count(*) from product");
			ps.setFetchSize(Integer.MIN_VALUE);
			rs = ps.executeQuery();
			try {
				rs.next();
				dbSize = rs.getInt(1);
			} finally {
				rs.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return dbSize;
	}
}
