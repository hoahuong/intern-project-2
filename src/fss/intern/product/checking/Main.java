package fss.intern.product.checking;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import fss.intern.product.checking.dao.ProductDAOImpl;
import fss.intern.product.checking.entities.Product;
import fss.intern.product.checking.service.ProductService;

/**
 * @author Huong
 *
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProductDAOImpl pd = new ProductDAOImpl();
		ProductService ps = new ProductService(pd);
		
		java.util.Properties properties = System.getProperties();
		
		// get Operating System home directory
	    String home = properties.get("user.home").toString();

	    // get Operating System separator
	    String separator = properties.get("file.separator").toString();

		String serverPath = "http://52.35.183.154/output/";
		String localPath = home + separator + "test" + separator; //"C:\\CSV\\";

		//This line use to insert to db after downloading the lastest csv file
//		ps.downloadCSVAndInsertToDB(serverPath, localPath);

		//download the lastest csv file and check data in csv files and data in db
//		List<Product> productList = ps.getAllProductFromDB(); //get all data from db -- be not used
//		if (productList.size() > 0) {
			ps.downloadAndCheck(serverPath, localPath, ps);
//		}
	}
}
