/**
 * 
 */
package fss.intern.product.checking.entities;

/**
 * @author Administrator
 *
 */
public class Product {
	
	//product id
	private int id;
	//row id of product in csv
	private int rowId;
	//product url
	private String pUrl;
	//product code
	private String pCode;
	//product name
	private String pName;
	
	public Product(int rowId, String pUrl, String pCode, String pName) {
		super();
		this.rowId = rowId;
		this.pUrl = pUrl;
		this.pCode = pCode;
		this.pName = pName;
	}
	public Product(String pUrl, String pCode, String pName) {
		super();
		this.pUrl = pUrl;
		this.pCode = pCode;
		this.pName = pName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getpUrl() {
		return pUrl;
	}
	public void setpUrl(String pUrl) {
		this.pUrl = pUrl;
	}
	public String getpCode() {
		return pCode;
	}
	public void setpCode(String pCode) {
		this.pCode = pCode;
	}
	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}

}
