# Inten Project 2 #

This project use to check the newest data of CSV with data in database.

### How do I get set up? ###

* Set up database:

	Run SQL script as below:
	```
	DROP TABLE IF EXISTS `product`;
	CREATE TABLE `product` (
	  `id` int(255) NOT NULL AUTO_INCREMENT,
	  `url` varchar(500) NOT NULL,
	  `product_code` varchar(255) NOT NULL,
	  `product_name` varchar(255) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `product_code` (`product_code`) USING BTREE,
	  KEY `product_url` (`url`(255)) USING BTREE,
	  KEY `product_name` (`product_name`) USING BTREE
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	```
	
* Required environment:

	- Java8: You can install at [here](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
	
* Execute file in folder: bin

* Execute program by command: ```java -jar product_tracking.jar```

* Summary functions:

	1/. To insert the lastest data of csv to DB, please uncomment below code in Main.java:
	
	```ps.downloadCSVAndInsertToDB(serverPath, localPath);```
	
	2/. To execute to check data of the lastest csv file, please use uncomment below code in Main.java:
	
	```java
	List<Product> productList = ps.getAllProduct_DB(); //get all data from db
	if (productList.size() > 0) {
		//download CSV file to check, if check completed --> auto-delete this csv file
		ps.downloadAndCheck(serverPath, localPath, productList, ps);
	}
	```
	3/. Others
	
	- You can reconfig path to store csv file at line in Main.java:
	
		```String localPath = System.getProperty("user.dir") + "/";  //this code will store csv file at project folder```
		
	- You can edit mail content at generateContentMail fuction in ProductService.java
	
	- You can edit mail address at line 372 in downloadAndCheck function in ProductService.java
	
	- In Windows7, if mail cannot use, please [read more at here](https://stackoverflow.com/questions/25341198/javax-mail-authenticationfailedexception-is-thrown-while-sending-email-in-java)